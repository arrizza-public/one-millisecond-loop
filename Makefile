.PHONY : all clean help  one-millisecond-loop one-millisecond-loop-init one-millisecond-loop-build one-millisecond-loop-link ut ut-init ut-build ut-link
#-- build all
all:  one-millisecond-loop one-millisecond-loop-init one-millisecond-loop-build one-millisecond-loop-link ut ut-init ut-build ut-link
#-- build one-millisecond-loop
one-millisecond-loop: one-millisecond-loop-init one-millisecond-loop-build one-millisecond-loop-link
#-- build ut
ut: ut-init ut-build ut-link

# ==== one-millisecond-loop

#-- one-millisecond-loop: initialize for debug build
one-millisecond-loop-init:
	@mkdir -p debug
	@mkdir -p debug/one-millisecond-loop-dir/lib
	@mkdir -p debug/one-millisecond-loop-dir/sample

-include debug/one-millisecond-loop-dir/lib/millisecond_loop.cpp.d
debug/one-millisecond-loop-dir/lib/millisecond_loop.cpp.o: lib/millisecond_loop.cpp
	c++ -MMD -c "-Ilib"  -std=c++20 -D_GNU_SOURCE  lib/millisecond_loop.cpp -o debug/one-millisecond-loop-dir/lib/millisecond_loop.cpp.o
-include debug/one-millisecond-loop-dir/sample/app.cpp.d
debug/one-millisecond-loop-dir/sample/app.cpp.o: sample/app.cpp
	c++ -MMD -c "-Ilib"  -std=c++20 -D_GNU_SOURCE  sample/app.cpp -o debug/one-millisecond-loop-dir/sample/app.cpp.o
-include debug/one-millisecond-loop-dir/sample/main.cpp.d
debug/one-millisecond-loop-dir/sample/main.cpp.o: sample/main.cpp
	c++ -MMD -c "-Ilib"  -std=c++20 -D_GNU_SOURCE  sample/main.cpp -o debug/one-millisecond-loop-dir/sample/main.cpp.o

#-- one-millisecond-loop: build source files
one-millisecond-loop-build: debug/one-millisecond-loop-dir/lib/millisecond_loop.cpp.o debug/one-millisecond-loop-dir/sample/app.cpp.o debug/one-millisecond-loop-dir/sample/main.cpp.o 

debug/one-millisecond-loop: debug/one-millisecond-loop-dir/lib/millisecond_loop.cpp.o debug/one-millisecond-loop-dir/sample/app.cpp.o debug/one-millisecond-loop-dir/sample/main.cpp.o 
	c++ debug/one-millisecond-loop-dir/lib/millisecond_loop.cpp.o debug/one-millisecond-loop-dir/sample/app.cpp.o debug/one-millisecond-loop-dir/sample/main.cpp.o     -o debug/one-millisecond-loop

#-- one-millisecond-loop: link
one-millisecond-loop-link: debug/one-millisecond-loop

# used to pass args in cpp and gtest cmd lines
%:
	@:
#-- one-millisecond-loop: run executable
one-millisecond-loop-run: one-millisecond-loop-link
	@debug/one-millisecond-loop $(filter-out $@,$(MAKECMDGOALS))

#-- one-millisecond-loop: clean files in this target
one-millisecond-loop-clean:
	rm -f debug/one-millisecond-loop-dir/lib/*.o
	rm -f debug/one-millisecond-loop-dir/lib/*.d
	rm -f debug/one-millisecond-loop-dir/sample/*.o
	rm -f debug/one-millisecond-loop-dir/sample/*.d
	rm -f debug/one-millisecond-loop

# ==== ut

#-- ut: initialize for debug build
ut-init:
	@mkdir -p debug
	@mkdir -p debug/ut-dir/ut

-include debug/ut-dir/ut/ut_test1.cpp.d
debug/ut-dir/ut/ut_test1.cpp.o: ut/ut_test1.cpp
	g++ -MMD -c "-Ilib" "-Isample"  -g -fdiagnostics-color=always -fprofile-arcs -ftest-coverage -DGTEST_HAS_PTHREAD=1 -std=gnu++20 -D_UCRT -D_GNU_SOURCE  ut/ut_test1.cpp -o debug/ut-dir/ut/ut_test1.cpp.o

#-- ut: build source files
ut-build: debug/ut-dir/ut/ut_test1.cpp.o 

debug/ut: debug/ut-dir/ut/ut_test1.cpp.o 
	g++ debug/ut-dir/ut/ut_test1.cpp.o    -lgtest -lpthread -lgcov -lgtest_main  -o debug/ut

#-- ut: link
ut-link: debug/ut ut-build

#-- ut: show coverage
ut-cov:
	gcovr --html-details --sort=uncovered-percent --print-summary -o debug/ut.html --filter lib 
	@echo "see debug/ut.html" for HTML report

#-- ut: run executable
ut-run: ut-link
	debug/ut $(filter-out $@,$(MAKECMDGOALS))

#-- ut: reset coverage info
ut-cov-reset:
	rm -f debug/ut-dir/ut/*.gcda

#-- ut: clean files in this target
ut-clean: ut-cov-reset
	rm -f debug/ut-dir/ut/*.o
	rm -f debug/ut-dir/ut/*.d
	rm -f debug/ut-dir/ut/*.gcno
	rm -f debug/ut
	rm -f debug/ut.html
	rm -f debug/ut.css
	rm -f debug/ut.**.html

#-- clean files
clean: one-millisecond-loop-clean ut-clean 

help:
	@printf "Available targets:\n"
	@printf "  [32;01mall                                [0m build all\n"
	@printf "  [32;01mclean                              [0m clean files\n"
	@printf "  [32;01mhelp                               [0m this help info\n"
	@printf "  [32;01mone-millisecond-loop               [0m build one-millisecond-loop\n"
	@printf "    [32;01mone-millisecond-loop-build         [0m one-millisecond-loop: build source files\n"
	@printf "    [32;01mone-millisecond-loop-clean         [0m one-millisecond-loop: clean files in this target\n"
	@printf "    [32;01mone-millisecond-loop-init          [0m one-millisecond-loop: initialize for debug build\n"
	@printf "    [32;01mone-millisecond-loop-link          [0m one-millisecond-loop: link\n"
	@printf "    [32;01mone-millisecond-loop-run           [0m one-millisecond-loop: run executable\n"
	@printf "  [32;01mut                                 [0m build ut\n"
	@printf "    [32;01mut-build                           [0m ut: build source files\n"
	@printf "    [32;01mut-clean                           [0m ut: clean files in this target\n"
	@printf "    [32;01mut-cov                             [0m ut: show coverage\n"
	@printf "    [32;01mut-cov-reset                       [0m ut: reset coverage info\n"
	@printf "    [32;01mut-init                            [0m ut: initialize for debug build\n"
	@printf "    [32;01mut-link                            [0m ut: link\n"
	@printf "    [32;01mut-run                             [0m ut: run executable\n"
	@printf "\n"

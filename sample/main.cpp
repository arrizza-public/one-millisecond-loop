#include "app.h"

// -------------------
auto main() -> int
{
    App app;
    app.do_millisecond_loop();

    return 0;
}

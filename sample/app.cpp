// ============================================================================
// Copyright(c) All Rights Reserved
//
// Summary: test an accurate millisecond loop; use on-the-fly stats to check the accuracy
// ============================================================================
#include "app.h"

#include <iomanip>
#include <iostream>
#include <thread>

// -------------------
App::App()
    : m_generator(m_rd())
    , m_distribution(1, 900)
    , m_stddev_num(0)
    , last_tick(0)
    , loop_index(0)
    , loop_max(0)
{
    m_stddev_last_mean = 0.0;
    m_stddev_last_sigma = 0.0;
    m_stddev_sigma = 0.0;
    m_stddev_mean = 0.0;
    m_stddev = 0.0;
}

// -------------------
// return true when loop is done
auto App::done() -> bool
{
    loop_index++;
    return loop_index > loop_max;
}

// -------------------
// run a series of trials and check the statistics
void App::do_millisecond_loop()
{
    // use 1000 for one second loop time
    loop_max = 1000;
    uint32_t trials = 180;
    // Note: loop_max * trials should be smaller than the size of durations!

    double overall_total_clock = 0.0;
    std::cout << "Per trial statistics:" << std::endl;
    std::cout << "            Avg (nS)     mS       err(mS)   err(uS)" << std::endl;

    double sum_err_us = 0.0;

    last_tick = now_in_ns();
    for (uint32_t i = 0; i < trials; ++i) {
        // run the loop loop_max times
        loop_index = 0;
        uint64_t start_clock = now_in_ns();
        do_loop();
        uint64_t end_clock = now_in_ns();

        // calculate the statistics for this trial
        int64_t avg_ns = end_clock - start_clock;
        double avg_ms = avg_ns / (double)MillisecondPerLoop::one_ms_in_ns;
        overall_total_clock += (end_clock - start_clock) / (double)MillisecondPerLoop::one_ms_in_ns;

        double err_ms = avg_ms - loop_max;
        double err_us = err_ms * 1000.0;

        sum_err_us += err_us;

        std::cout.precision(3);
        std::cout << "   [" << std::setw(4) << i << "] ";
        std::cout << std::setw(10) << avg_ns << "  ";
        std::cout << std::setw(9) << std::fixed << avg_ms << " ";
        std::cout << std::setw(9) << std::fixed << err_ms << " ";
        std::cout << std::setw(9) << std::fixed << err_us << " ";
        std::cout << std::setw(9) << std::fixed << m_dbg;
        std::cout << std::endl;
    }

    // trials are done, calculate stats for the overall run
    std::cout << "\n";
    std::cout << "Overall statistics:\n";
    std::cout << "   Expected total time: " << std::setw(9) << float(trials * loop_max) << " mS\n";
    std::cout << "   Actual total time  : " << std::setw(9) << overall_total_clock << " mS\n";

    std::cout.precision(3);
    std::cout << "   loop StdDev        : " << std::setw(9) << m_stddev << " uS" << std::endl;
    std::cout << "   loop Mean          : " << std::setw(9) << m_stddev_mean << " uS" << std::endl;
    std::cout << "   loop Overall       : " << std::setw(9) << m_stddev_mean << " uS +/- " << (2.0 * m_stddev)
              << " uS (with 95% confidence)" << std::endl;

    double avg_err_us = sum_err_us / trials;
    std::cout << "   average error      : " << std::setw(9) << avg_err_us << " uS" << std::endl;
}

// -------------------
// the actual loop doing the (simulated) work
void App::on_tick()
{
    // calculate the duration of the last tick
    int64_t tick = now_in_ns();
    double duration = (tick - last_tick) / 1000.0; // duration is in uS
    last_tick = tick;

    // calculate ongoing stddev
    m_stddev_num += 1;
    if (m_stddev_num == 1) {
        m_stddev_last_mean = duration;
        m_stddev_mean = duration;
        m_stddev_last_sigma = 0.0;
    } else {
        m_stddev_mean = m_stddev_last_mean + ((duration - m_stddev_last_mean) / m_stddev_num);
        m_stddev_sigma = m_stddev_last_sigma + ((duration - m_stddev_last_mean) * (duration - m_stddev_mean));

        // set up for next iteration
        m_stddev_last_mean = m_stddev_mean;
        m_stddev_last_sigma = m_stddev_sigma;
    }

    m_stddev = ::sqrt(m_stddev_sigma / (m_stddev_num - 1));

    // TEST only: simulate the work done in every tick by waiting a random amount of time
    auto random_time = std::chrono::microseconds(m_distribution(m_generator));
    // uncomment to debug
    // std::cout << random_time.count() << std::endl;
    std::this_thread::sleep_for(std::chrono::microseconds(m_distribution(m_generator)));
}

// -------------------
auto App::now_in_ns() -> uint64_t
{
    return std::chrono::duration_cast<std::chrono::nanoseconds>(
        std::chrono::system_clock::now().time_since_epoch())
        .count();
}

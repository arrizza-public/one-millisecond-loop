#ifndef APP_H
#define APP_H
// ============================================================================
// Copyright(c) All Rights Reserved
//
// Summary: test an accurate millisecond loop; use on-the-fly stats to check the accuracy
// ============================================================================
#include "../lib/millisecond_loop.h"

#include <random>

class App : public MillisecondPerLoop {
public:
    App();
    void do_millisecond_loop();
    auto now_in_ns() -> uint64_t;

    auto done() -> bool override;
    void on_tick() final;

private:
    // for stddev calculation
    int64_t m_stddev_num { 0 };
    double m_stddev_last_mean { 0.0 };
    double m_stddev_last_sigma { 0.0 };
    double m_stddev_sigma { 0.0 };
    double m_stddev_mean { 0.0 };
    double m_stddev { 0.0 };

    int64_t last_tick { 0 };

    int loop_index { 0 };
    int loop_max { 0 };

    std::random_device m_rd;
    std::mt19937 m_generator;
    std::uniform_int_distribution<> m_distribution;
};

#endif // APP_H

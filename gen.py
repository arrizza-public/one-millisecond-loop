from pyalamake import alamake

# === cpip
# pkg = alamake.find_package('cpip.logger')

# === C++ tgt
tgt = alamake.create('one-millisecond-loop', 'cpp')
tgt.add_sources([
    'lib/millisecond_loop.cpp',

    'sample/app.cpp',
    'sample/main.cpp',
])
tgt.add_include_directories(['lib'])
# tgt.add_sources(pkg.src)
# tgt.add_include_directories(pkg.include_dir)

# === gtest
ut = alamake.create('ut', 'gtest')

# add homebrew dirs if running on macOS
ut.add_homebrew()

ut.add_include_directories([
    'lib',
    'sample',
])
ut.add_sources([
    'ut/ut_test1.cpp',
])
ut.add_link_libraries('gtest_main')
ut.add_coverage(['lib'])
# ut.add_sources(pkg.src)
# ut.add_include_directories(pkg.include_dir)

# === generate makefile for all targets
alamake.makefile()

#include "millisecond_loop.h"

#include <chrono>
#include <thread>

// -------------------
void MillisecondPerLoop::do_loop()
{
    int64_t time_to_wait = 0;
    int64_t now;
    int64_t curr_sum_err = 0;
    int64_t curr_sum_count = 0;

    last_clock = get_current_clock_ns();
    next_clock = last_clock + one_ms_in_ns;

    while (!done()) {
        // Assume on_tick takes less than 1 ms to run
        on_tick();

        // calculate the time to wait from now until the next tick time
        time_to_wait = next_clock - get_current_clock_ns() - avg_err;

        // check if we're already past the 1ms time interval
        if (time_to_wait > 0) {
            // no, so wait that many ns
            std::this_thread::sleep_for(std::chrono::nanoseconds(time_to_wait));
        }

        ++m_tick;

        // calculate a periodically updated average error
        now = get_current_clock_ns();
        ++curr_sum_count;
        curr_sum_err += now - next_clock; // elapsed time in nS
        if (curr_sum_count > 100) {
            // update the average error applied per loop
            avg_err += curr_sum_err / curr_sum_count;
            curr_sum_err = 0;
            curr_sum_count = 0;
        }
        // for debug purposes
        m_dbg = avg_err;

        last_clock = now;
        next_clock += one_ms_in_ns;
    }
}

// -------------------
int64_t MillisecondPerLoop::get_current_clock_ns()
{
    return std::chrono::duration_cast<std::chrono::nanoseconds>(
        std::chrono::system_clock::now().time_since_epoch())
        .count();
}

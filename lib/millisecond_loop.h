#ifndef MILLISECOND_PER_LOOP_H
#define MILLISECOND_PER_LOOP_H

#include <cstdint>

// -------------------
//! run a loop at 1mS
class MillisecondPerLoop {
public:
    //! run the loop
    void do_loop();
    //! callback when the 1mS timeout occurs
    virtual void on_tick() = 0;
    //! return True to indicate to the loop to exit
    //! @return True to exit, False otherwise
    virtual auto done() -> bool = 0;

    //! number of nanoseconds in a millisecond
    static constexpr int64_t one_ms_in_ns = 1'000'000L;

protected:
    //! current tick time
    int64_t m_tick = 0;
    //! for debug purposes only
    int64_t m_dbg = 0; // for debug only

private:
    //! get current RTC clock value in nanoseconds
    //! @return RTC lock value
    static auto get_current_clock_ns() -> int64_t;

    //! next expected clock time
    int64_t next_clock {};
    //! last actual clock time
    int64_t last_clock {};
    //! current average error in nanoseconds
    //! use a typical value; not needed if the loop
    //! is run for a long time
    int64_t avg_err = 50'000;
};

#endif // MILLISECOND_PER_LOOP_H
